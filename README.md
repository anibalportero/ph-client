# PantacorHub Client

This client has been implemented using an event loop that, depending on the running state of a state machine, performs requests to both Pantavisor and Pantacor Hub to make communication between each other possible.

## Pros and cons

A list of pros and cons to decide what to do with this.

Pros:
* only one thread for all cloud communications thanks to event loop
* well tested libraries for easier development: event loop, json serializer and deserializer, http client, tls
* independent from Pantavisor: we can work locally with pantabox, at some point add the client container, claim the device, continue working from cloud side, remove the client container after that, add it again...
* more flexibility: developers can socat the pv-ctrl socket into a TCP socket and run the client directly in their host computers
* faster Pantavisor pv-ctrl: Pantavisor main thread will not be blocked by HTTP client as it will be working in local mode
* no crashes
* no memory leaks

Cons:
* bigger size: 1.5 MB static binary for aarch64
* more memory compsumption: not sure about this one yet as we will avoid the push logger threads and hopefully save some of the main thread memory
* lots of work to do: mainly updates, logs and integration for legacy devices

## How to test from your host computer

In the host computer:

```
cargo build --release
./target/release/ph-client -h
```

In the device:

```
ssh -p 8222 pvr-sdk@192.168.1.122
socat TCP-LISTEN:12345,fork,reuseaddr UNIX-CONNECT:/pantavisor/pv-ctrl
```

## How to test from your Pantavisor device

TODO

## TODO list

* implement idle state: update (download files)
* implement idle state: log push
* save/load creds and revs in functions in pantavisor and remote
* improve timeout to remove remote and pvcontrol requests
* refactor main to make it more legible
* use pv-ctrl-log
* integration with pv-avahi id/challenge
* add HTTP error code constants (or functions)
* move request enum out of endpoint
* try to reduce size and memory consumption
* make everything configurable taking pantahub.config as example
* upload and download objects using stream instead of loading in memory
* script to upgrade from old pantavisor checkout to pantavisor+client
* use legacy configurations and credentials from old pantavisor devices

/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use mio::{event::Event, Registry};
use mio_httpc::{Call, CallBuilder, CallRef, Httpc, HttpcCfg, RecvState, SendState, SimpleCall};
use std::collections::HashMap;

pub struct Endpoint {
    htp: Httpc,
    calls: HashMap<CallRef, SimpleCall>,
    calls_stream: HashMap<CallRef, (Call, bool)>,
}

impl Endpoint {
    pub fn new() -> Endpoint {
        if let Err(e) = HttpcCfg::certs_from_path("/certs") {
            println!("could not open certs: {}", e);
        };
        Endpoint {
            htp: Httpc::new(11, None),
            calls: HashMap::new(),
            calls_stream: HashMap::new(),
        }
    }

    pub fn check_timeout(&mut self) {
        for cref in self.htp.timeout().into_iter() {
            println!("{:?} timed out", cref);
            if let Some(c) = self.calls.remove(&cref) {
                c.abort(&mut self.htp);
            }
            if let Some(c) = self.calls_stream.remove(&cref) {
                c.0.simple().abort(&mut self.htp);
            }
        }
    }

    pub fn build_url_http(&self, host: &str, port: u16, path: &str) -> String {
        CallBuilder::new()
            .host(host)
            .port(port)
            .path(path)
            .get_url()
    }

    pub fn build_url_https(&self, host: &str, port: u16, path: &str) -> String {
        CallBuilder::new()
            .https()
            .host(host)
            .port(port)
            .path(path)
            .get_url()
    }
}

impl Endpoint {
    pub fn is_pending(&self, cref: &CallRef) -> bool {
        self.calls.contains_key(&cref) && self.calls_stream.contains_key(&cref)
    }

    pub fn send_post(
        &mut self,
        url: &str,
        headers: &Vec<(&str, &str)>,
        body: &str,
        timeout: u64,
        reg: &Registry,
    ) -> Option<CallRef> {
        println!("POST {} HTTP/1.1", url);
        println!("{:?}", headers);
        println!("{}", body);
        let mut builder = CallBuilder::post(body.as_bytes().to_vec());
        for header in headers.iter() {
            builder.header(&header.0, &header.1);
        }

        match builder.timeout_ms(timeout).url(&url) {
            Ok(c) => match c.call(&mut self.htp, reg) {
                Ok(c) => {
                    let cref = c.get_ref().clone();
                    self.calls.insert(c.get_ref(), c.simple());
                    println!("{:?} inserted", cref);
                    return Some(cref);
                }
                Err(e) => println!("post call failed {}", e),
            },
            Err(e) => println!("post build failed {}", e),
        }
        None
    }

    pub fn send_put(
        &mut self,
        url: &str,
        headers: &Vec<(&str, &str)>,
        body: &str,
        timeout: u64,
        reg: &Registry,
    ) -> Option<CallRef> {
        println!("PUT {} HTTP/1.1", url);
        println!("{:?}", headers);
        println!("{}", body);
        let mut builder = CallBuilder::put(body.as_bytes().to_vec());
        for header in headers.iter() {
            builder.header(&header.0, &header.1);
        }

        match builder.timeout_ms(timeout).url(&url) {
            Ok(c) => match c.call(&mut self.htp, reg) {
                Ok(c) => {
                    let cref = c.get_ref().clone();
                    self.calls.insert(c.get_ref(), c.simple());
                    println!("{:?} inserted", cref);
                    return Some(cref);
                }
                Err(e) => println!("put call failed {}", e),
            },
            Err(e) => println!("put build failed {}", e),
        }
        None
    }

    pub fn send_get(
        &mut self,
        url: &str,
        headers: &Vec<(&str, &str)>,
        timeout: u64,
        reg: &Registry,
    ) -> Option<CallRef> {
        println!("GET {} HTTP/1.1", url);
        println!("{:?}", headers);
        let mut builder = CallBuilder::get();
        for header in headers.iter() {
            builder.header(&header.0, &header.1);
        }
        match builder.timeout_ms(timeout).url(&url) {
            Ok(c) => match c.call(&mut self.htp, reg) {
                Ok(c) => {
                    let cref = c.get_ref().clone();
                    self.calls.insert(c.get_ref(), c.simple());
                    println!("{:?} inserted", cref);
                    return Some(cref);
                }
                Err(e) => println!("get call failed {}", e),
            },
            Err(e) => println!("get build failed {}", e),
        }
        None
    }

    pub fn recv(&mut self, ev: &Event, reg: &Registry, rref: &CallRef) -> Option<(u16, Vec<u8>)> {
        let cref = self.htp.event(&ev);
        if let Some(c) = self.calls.get_mut(rref) {
            if c.is_call(&cref) {
                match c.perform(&mut self.htp, reg) {
                    Ok(false) => (),
                    Ok(true) => {
                        println!("received response for {:?}", cref);
                        if let Some(c) = self.calls.remove(rref) {
                            if let Some((r, b)) = c.finish() {
                                if let Ok(s) = String::from_utf8(b.clone()) {
                                    println!("HTTP/1.1 {}", r.status);
                                    println!("{}", r.headers());
                                    println!("{:?}", s);
                                }
                                return Some((r.status, b));
                            } else {
                                println!("could not finish call");
                            }
                        } else {
                            println!("{:?} not found", rref);
                        }
                    }
                    Err(e) => {
                        println!("req failed: {}", e);
                    }
                }
            }
        }

        None
    }
}

impl Endpoint {
    pub fn send_get_stream(
        &mut self,
        url: &str,
        headers: &Vec<(&str, &str)>,
        timeout: u64,
        reg: &Registry,
    ) -> Option<CallRef> {
        println!("GET {} HTTP/1.1", url);
        println!("{:?}", headers);
        let mut builder = CallBuilder::get();
        for header in headers.iter() {
            builder.header(&header.0, &header.1);
        }
        match builder.timeout_ms(timeout).url(&url) {
            Ok(c) => match c.call(&mut self.htp, reg) {
                Ok(c) => {
                    let cref = c.get_ref().clone();
                    self.calls_stream.insert(c.get_ref(), (c, false));
                    println!("{:?} inserted", cref);
                    return Some(cref);
                }
                Err(e) => println!("get call failed {}", e),
            },
            Err(e) => println!("get build failed {}", e),
        }
        None
    }

    pub fn recving_stream(&mut self, ev: &Event, reg: &Registry, rref: &CallRef) -> bool {
        if let Some(c) = self.calls_stream.get_mut(rref) {
            if c.1 {
                return true;
            }

            if let Some(cref) = self.htp.event(&ev) {
                if c.0.is_ref(cref) {
                    match self.htp.call_send(&reg, &mut c.0, None) {
                        SendState::Receiving => {
                            c.1 = true;
                            return true;
                        }
                        _ => println!("unexpected send state"),
                    }
                }
            }
        }
        false
    }

    pub fn send_put_stream(
        &mut self,
        url: &str,
        headers: &Vec<(&str, &str)>,
        timeout: u64,
        reg: &Registry,
    ) -> Option<CallRef> {
        println!("PUT {} HTTP/1.1", url);
        println!("{:?}", headers);
        let mut builder = CallBuilder::put(vec![]);
        for header in headers.iter() {
            builder.header(&header.0, &header.1);
        }

        match builder.timeout_ms(timeout).url(&url) {
            Ok(c) => match c.call(&mut self.htp, reg) {
                Ok(c) => {
                    let cref = c.get_ref().clone();
                    self.calls_stream.insert(c.get_ref(), (c, false));
                    println!("{:?} inserted", cref);
                    return Some(cref);
                }
                Err(e) => println!("put call failed {}", e),
            },
            Err(e) => println!("put build failed {}", e),
        }
        None
    }

    pub fn sending_stream(
        &mut self,
        ev: &Event,
        reg: &Registry,
        iref: &CallRef,
        oref: &CallRef,
    ) -> bool {
        let mut buf = Vec::new();
        if let Some(cref) = self.htp.event(&ev) {
            if let Some(oc) = self.calls_stream.get(&oref) {
                if oc.0.is_ref(cref) {
                    if let Some(ic) = self.calls_stream.get_mut(&iref) {
                        match self.htp.call_recv(&reg, &mut ic.0, Some(&mut buf)) {
                            RecvState::Done => (),
                            _ => println!("unexpected recv state"),
                        }
                    }
                }
            }
            if let Some(oc) = self.calls_stream.get_mut(&oref) {
                match self.htp.call_send(&reg, &mut oc.0, Some(&mut buf)) {
                    SendState::Done => return true,
                    _ => println!("unexpected send state"),
                }
            }
        }
        false
    }
}

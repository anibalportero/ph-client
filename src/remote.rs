/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use mio::{event::Event, Registry};
use mio_httpc::CallRef;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::endpoint::Endpoint;

pub enum ObjectStatus {
    Unknown,
    Synced,
    Unsynced(String),
}

pub struct Remote {
    config: Config,
    creds: Creds,
    bearer: Bearer,
    requests: HashMap<Request, CallRef>,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    host: String,
    port: u16,
    timeout: u64,
}

impl Config {
    pub fn new() -> Config {
        Config {
            host: "api.pantahub.com".to_string(),
            port: 443,
            timeout: 5000,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
enum Request {
    PostRegister,
    PostAuth,
    GetDevice,
    PostObject(String),
    PutObject(String),
    PostStep,
    PutDevmeta,
    GetUsrmeta,
}

impl Remote {
    pub fn new(config: Config, varpath: &str) -> Remote {
        let mut creds = Creds::new();
        creds.load(&varpath);
        Remote {
            config,
            creds,
            bearer: Bearer::new(),
            requests: HashMap::new(),
        }
    }

    pub fn reset_bearer(&mut self) {
        self.bearer.reset();
    }

    pub fn is_registered(&self) -> bool {
        !self.creds.id.is_empty() && !self.creds.prn.is_empty() && !self.creds.secret.is_empty()
    }

    pub fn is_authenticated(&self) -> bool {
        !self.bearer.token.is_empty()
    }

    pub fn is_owned(&self) -> bool {
        !self.creds.owner.is_empty()
    }
}

impl Remote {
    pub fn send_post_register(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::PostRegister, end) {
            return;
        }

        let url = end.build_url_https(&self.config.host, self.config.port, "devices");
        if let Some(cref) = end.send_post(
            &url,
            &vec![("Content-type", "application/json")],
            "",
            self.config.timeout,
            &reg,
        ) {
            self.requests.insert(Request::PostRegister, cref);
        }
    }

    pub fn send_post_auth(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::PostAuth, end) {
            return;
        }

        let url = end.build_url_https(&self.config.host, self.config.port, "auth/login");
        let body = self.build_body_auth();
        if let Some(cref) = end.send_post(
            &url,
            &vec![("Content-type", "application/json")],
            &body,
            self.config.timeout,
            &reg,
        ) {
            self.requests.insert(Request::PostAuth, cref);
        }
    }

    pub fn send_get_device(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GetDevice, end) {
            return;
        }

        let mut path = "devices/".to_owned();
        path.push_str(&self.creds.id);
        let url = end.build_url_https(&self.config.host, self.config.port, &path);
        let bearer = &self.build_bearer();
        if let Some(cref) = end.send_get(
            &url,
            &vec![("authorization", &bearer)],
            self.config.timeout,
            &reg,
        ) {
            self.requests.insert(Request::GetDevice, cref);
        }
    }

    pub fn send_post_objects(
        &mut self,
        sha256: &str,
        name: &str,
        size: &str,
        reg: &Registry,
        end: &mut Endpoint,
    ) {
        if self.is_pending(&Request::PostObject(sha256.to_string()), end) {
            return;
        }

        let url = end.build_url_https(&self.config.host, self.config.port, "objects");
        let bearer = &self.build_bearer();
        let body = self.build_body_object(&sha256, &name, &size);
        if let Some(cref) = end.send_post(
            &url,
            &vec![
                ("authorization", &bearer),
                ("Content-type", "application/json"),
            ],
            &body,
            self.config.timeout,
            &reg,
        ) {
            self.requests
                .insert(Request::PostObject(sha256.to_string()), cref);
        }
    }

    pub fn send_post_step(&mut self, step: &str, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::PostStep, end) {
            return;
        }

        let url = end.build_url_https(&self.config.host, self.config.port, "trails");
        let bearer = &self.build_bearer();
        if let Some(cref) = end.send_post(
            &url,
            &vec![("authorization", &bearer)],
            &step,
            self.config.timeout,
            &reg,
        ) {
            self.requests.insert(Request::PostStep, cref);
        }
    }

    pub fn send_put_object_stream(
        &mut self,
        sha256: &str,
        puturl: &str,
        size: &str,
        reg: &Registry,
        end: &mut Endpoint,
    ) {
        if self.is_pending(&Request::PutObject(sha256.to_string()), end) {
            return;
        }

        let url = end.build_url_https(&self.config.host, self.config.port, &puturl);

        if let Some(cref) = end.send_put_stream(
            &url,
            &vec![
                ("Content-type", "application/json"),
                ("content-length", &size),
            ],
            self.config.timeout,
            &reg,
        ) {
            self.requests
                .insert(Request::PutObject(sha256.to_string()), cref.clone());
        }
    }

    pub fn send_put_devmeta(&mut self, devmeta: &str, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::PutDevmeta, end) {
            return;
        }

        let mut path = "devices/".to_owned();
        path.push_str(&self.creds.id);
        path.push_str("/device-meta");
        let url = end.build_url_https(&self.config.host, self.config.port, &path);
        let bearer = &self.build_bearer();
        if let Some(cref) = end.send_put(
            &url,
            &vec![
                ("authorization", &bearer),
                ("Content-type", "application/json"),
            ],
            &devmeta,
            self.config.timeout,
            &reg,
        ) {
            self.requests.insert(Request::PutDevmeta, cref);
        }
    }

    pub fn send_get_usrmeta(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GetUsrmeta, end) {
            return;
        }

        let mut path = "devices/".to_owned();
        path.push_str(&self.creds.id);
        let url = end.build_url_https(&self.config.host, self.config.port, &path);
        let bearer = &self.build_bearer();
        if let Some(cref) = end.send_get(
            &url,
            &vec![("authorization", &bearer)],
            self.config.timeout,
            &reg,
        ) {
            self.requests.insert(Request::GetUsrmeta, cref);
        }
    }

    fn is_pending(&mut self, req: &Request, end: &mut Endpoint) -> bool {
        if let Some(&cref) = self.requests.get(&req) {
            return end.is_pending(&cref);
        }
        false
    }

    fn build_body_auth(&mut self) -> String {
        #[derive(Serialize)]
        struct Auth {
            password: String,
            username: String,
        }

        let auth = Auth {
            password: self.creds.secret.clone(),
            username: self.creds.prn.clone(),
        };

        match serde_json::to_string(&auth) {
            Ok(b) => return b,
            Err(e) => println!("could not serialize auth body: {}", e),
        };

        "".to_string()
    }

    fn build_bearer(&mut self) -> String {
        let mut bearer = "Bearer ".to_owned();
        bearer.push_str(&self.bearer.token);
        bearer
    }

    fn build_body_object(&self, sha256: &str, name: &str, size: &str) -> String {
        #[derive(Serialize)]
        struct ObjectPost<'a> {
            sha256sum: &'a str,
            objectname: &'a str,
            size: &'a str,
        }

        let object = ObjectPost {
            sha256sum: sha256,
            objectname: name,
            size: size,
        };

        match serde_json::to_string(&object) {
            Ok(b) => return b,
            Err(e) => println!("could not serialize object body: {}", e),
        };

        "".to_string()
    }
}

impl Remote {
    pub fn recv_post_register(
        &mut self,
        event: &Event,
        reg: &Registry,
        end: &mut Endpoint,
        varpath: &str,
    ) {
        if let Some(&cref) = self.requests.get(&Request::PostRegister) {
            if let Some((r, b)) = end.recv(&event, &reg, &cref) {
                if r == 200 {
                    self.creds.parse(&b);
                    self.creds.save(&varpath);
                }
                self.requests.remove(&Request::PostRegister);
            }
        }
    }

    pub fn recv_post_auth(&mut self, event: &Event, reg: &Registry, end: &mut Endpoint) {
        if let Some(&cref) = self.requests.get(&Request::PostAuth) {
            if let Some((r, b)) = end.recv(&event, &reg, &cref) {
                if r == 200 {
                    self.bearer.parse(&b);
                }
                self.requests.remove(&Request::PostAuth);
            }
        }
    }

    pub fn recv_get_device(
        &mut self,
        event: &Event,
        reg: &Registry,
        end: &mut Endpoint,
        varpath: &str,
    ) {
        if let Some(&cref) = self.requests.get(&Request::GetDevice) {
            if let Some((r, b)) = end.recv(&event, &reg, &cref) {
                if r == 200 {
                    self.creds.parse(&b);
                    self.creds.save(&varpath);
                }
                self.requests.remove(&Request::GetDevice);
            }
        }
    }

    pub fn recv_post_objects(
        &mut self,
        sha256: &str,
        event: &Event,
        reg: &Registry,
        end: &mut Endpoint,
    ) -> ObjectStatus {
        #[derive(Deserialize, Debug)]
        struct RegisteredObject {
            #[serde(rename = "signed-puturl")]
            puturl: String,
        }
        if let Some(&cref) = self.requests.get(&Request::PostObject(sha256.to_string())) {
            if let Some((r, b)) = end.recv(&event, &reg, &cref) {
                if r == 200 {
                    match serde_json::from_slice(&b) {
                        Ok(b) => {
                            let ro: RegisteredObject = b;
                            return ObjectStatus::Unsynced(ro.puturl);
                        }
                        Err(e) => println!("could not parse registered object: {}", e),
                    }
                } else if r == 409 {
                    return ObjectStatus::Synced;
                }
                self.requests
                    .remove(&Request::PostObject(sha256.to_string()));
            }
        }
        ObjectStatus::Unknown
    }

    pub fn recv_post_step(&mut self, event: &Event, reg: &Registry, end: &mut Endpoint) -> bool {
        if let Some(&cref) = self.requests.get(&Request::PostStep) {
            if let Some((r, _b)) = end.recv(&event, &reg, &cref) {
                if r == 200 {
                    return true;
                }
                self.requests.remove(&Request::PostStep);
            }
        }
        false
    }

    pub fn sending_put_object_stream(
        &mut self,
        iref: &CallRef,
        sha256: &str,
        end: &mut Endpoint,
        ev: &Event,
        reg: &Registry,
    ) -> bool {
        if let Some(&cref) = self.requests.get(&Request::PutObject(sha256.to_string())) {
            return end.sending_stream(&ev, &reg, &iref, &cref);
        }
        false
    }

    pub fn free_put_object_stream(&mut self, sha256: &str) {
        self.requests
            .remove(&Request::PutObject(sha256.to_string()));
    }

    pub fn recv_put_devmeta(&mut self, event: &Event, reg: &Registry, end: &mut Endpoint) {
        if let Some(&cref) = self.requests.get(&Request::PutDevmeta) {
            if let Some((_r, _b)) = end.recv(&event, &reg, &cref) {
                self.requests.remove(&Request::PutDevmeta);
            }
        }
    }

    pub fn recv_get_usrmeta(
        &mut self,
        event: &Event,
        reg: &Registry,
        end: &mut Endpoint,
    ) -> Option<String> {
        if let Some(&cref) = self.requests.get(&Request::GetUsrmeta) {
            if let Some((r, b)) = end.recv(&event, &reg, &cref) {
                if r == 200 {
                    match String::from_utf8(b.clone()) {
                        Ok(s) => return Some(s),
                        Err(e) => println!("could not get usrmeta: {}", e),
                    }
                }
                self.requests.remove(&Request::GetUsrmeta);
            }
        }
        None
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Creds {
    owner: String,
    id: String,
    prn: String,
    secret: String,
}

impl Creds {
    pub fn new() -> Creds {
        Creds {
            owner: "".to_owned(),
            id: "".to_owned(),
            prn: "".to_owned(),
            secret: "".to_owned(),
        }
    }

    fn parse(&mut self, json: &Vec<u8>) {
        match serde_json::from_slice(json) {
            Ok(c) => {
                let tmp: Creds = c;
                *self = tmp;
            }
            Err(e) => println!("could not parse creds: {}", e),
        }
    }
}

#[derive(Deserialize, Debug)]
struct Bearer {
    token: String,
}

impl Bearer {
    fn new() -> Bearer {
        Bearer {
            token: "".to_owned(),
        }
    }

    fn parse(&mut self, json: &Vec<u8>) {
        match serde_json::from_slice(json) {
            Ok(b) => {
                let tmp: Bearer = b;
                *self = tmp;
            }
            Err(e) => println!("could not parse bearer: {}", e),
        }
    }

    fn reset(&mut self) {
        self.token.clear();
    }
}

/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use serde::Deserialize;
use std::fs::File;
use std::path::Path;

use crate::pantavisor::Revisions;
use crate::pvcontrol::Config as PVControlConfig;
use crate::remote::Config as RemoteConfig;
use crate::remote::Creds;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub remote: RemoteConfig,
    pub pvcontrol: PVControlConfig,
}

impl Config {
    pub fn new() -> Config {
        Config {
            remote: RemoteConfig::new(),
            pvcontrol: PVControlConfig::new(),
        }
    }

    pub fn load(&mut self, path: &str) {
        println!("loading config from {}", path);

        match File::open(Path::new(path)) {
            Ok(f) => match serde_json::from_reader(f) {
                Ok(c) => {
                    let tmp: Config = c;
                    *self = tmp;
                }
                Err(e) => println!("could not parse config: {}", e),
            },
            Err(e) => println!("could not open config: {}", e),
        }
    }

    pub fn print(&self) {
        println!("{:?}", self);
    }
}

impl Creds {
    pub fn load(&mut self, varpath: &str) {
        let mut path = varpath.to_string();
        path.push_str("creds.json");
        println!("loading creds from {}...", &path);
        match File::open(Path::new(&path)) {
            Ok(f) => match serde_json::from_reader(f) {
                Ok(c) => {
                    let tmp: Creds = c;
                    *self = tmp;
                }
                Err(e) => println!("could not parse creds: {}", e),
            },
            Err(e) => println!("could not open creds: {}", e),
        }
    }

    pub fn save(&self, varpath: &str) {
        let mut path = varpath.to_string();
        path.push_str("creds.json");
        println!("saving creds in {}...", &path);
        match File::create(Path::new(&path)) {
            Ok(f) => {
                if let Err(e) = serde_json::to_writer(f, self) {
                    println!("could not compose creds: {}", e);
                }
            }
            Err(e) => println!("could not open creds: {}", e),
        }
    }
}

impl Revisions {
    pub fn load(&mut self, varpath: &str) {
        let mut path = varpath.to_string();
        path.push_str("revs.json");
        println!("loading revisions from {}...", &path);
        match File::open(Path::new(&path)) {
            Ok(f) => match serde_json::from_reader(f) {
                Ok(s) => {
                    let tmp: Revisions = s;
                    *self = tmp;
                }
                Err(e) => println!("could not parse revisions: {}", e),
            },
            Err(e) => println!("could not open revisions: {}", e),
        }
    }

    pub fn save(&self, varpath: &str) {
        let mut path = varpath.to_string();
        path.push_str("revs.json");
        println!("saving revisions in {}...", &path);
        match File::create(Path::new(&path)) {
            Ok(f) => {
                if let Err(e) = serde_json::to_writer(f, self) {
                    println!("could not compose revisions: {}", e);
                }
            }
            Err(e) => println!("could not open revisions: {}", e),
        }
    }
}

/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use mio::{event::Event, Registry};

use crate::endpoint::Endpoint;
use crate::pantavisor::Pantavisor;
use crate::pvcontrol::Config as PVControlConfig;
use crate::pvcontrol::PVControl;
use crate::remote::Config as RemoteConfig;
use crate::remote::{ObjectStatus, Remote};

#[derive(Debug)]
enum State {
    Init,
    Register,
    Auth,
    Claim,
    LoadObjects,
    LoadStep,
    FactoryStep,
    GotoStep,
    SyncObjects,
    SyncStep,
    Idle,
}

pub struct Client {
    varpath: String,
    state: State,
    end: Endpoint,
    re: Remote,
    pc: PVControl,
    pv: Pantavisor,
}

impl Client {
    pub fn new(rconfig: RemoteConfig, pcconfig: PVControlConfig, varpath: &str) -> Client {
        Client {
            varpath: varpath.to_string(),
            state: State::Init,
            end: Endpoint::new(),
            re: Remote::new(rconfig, &varpath),
            pc: PVControl::new(pcconfig),
            pv: Pantavisor::new(&varpath),
        }
    }
}

impl Client {
    pub fn init_state(&mut self, reg: &Registry) {
        println!("progressing to state {:?}", self.state);
        match self.state {
            State::Init => (),
            State::Register => self.init_register(&reg),
            State::Auth => self.init_auth(&reg),
            State::Claim => self.init_claim(&reg),
            State::LoadObjects => self.init_load_objects(&reg),
            State::LoadStep => self.init_load_step(&reg),
            State::FactoryStep => self.init_factory_step(&reg),
            State::GotoStep => self.init_goto_step(&reg),
            State::SyncObjects => self.init_sync_objects(&reg),
            State::SyncStep => self.init_sync_step(&reg),
            State::Idle => self.init_idle(&reg),
        }
    }

    pub fn check_timeout(&mut self) {
        self.end.check_timeout();
    }

    pub fn send_state(&mut self, reg: &Registry) {
        match self.state {
            State::Init => (),
            State::Register => self.send_register(&reg),
            State::Auth => self.send_auth(&reg),
            State::Claim => self.send_claim(&reg),
            State::LoadObjects => self.send_load_objects(&reg),
            State::LoadStep => self.send_load_step(&reg),
            State::FactoryStep => self.send_factory_step(&reg),
            State::GotoStep => self.send_goto_step(&reg),
            State::SyncObjects => self.send_sync_objects(&reg),
            State::SyncStep => self.send_sync_step(&reg),
            State::Idle => self.send_idle(&reg),
        }
    }

    pub fn recv_state(&mut self, ev: &Event, reg: &Registry) {
        match self.state {
            State::Init => (),
            State::Register => self.recv_register(&ev, &reg),
            State::Auth => self.recv_auth(&ev, &reg),
            State::Claim => self.recv_claim(&ev, &reg),
            State::LoadObjects => self.recv_load_objects(&ev, &reg),
            State::LoadStep => self.recv_load_step(&ev, &reg),
            State::FactoryStep => self.recv_factory_step(&ev, &reg),
            State::GotoStep => self.recv_goto_step(&ev, &reg),
            State::SyncObjects => self.recv_sync_objects(&ev, &reg),
            State::SyncStep => self.recv_sync_step(&ev, &reg),
            State::Idle => self.recv_idle(&ev, &reg),
        }
    }

    pub fn eval_state(&mut self) -> bool {
        match self.state {
            State::Init => self.eval_init(),
            State::Register => self.eval_register(),
            State::Auth => self.eval_auth(),
            State::Claim => self.eval_claim(),
            State::LoadObjects => self.eval_load_objects(),
            State::LoadStep => self.eval_load_step(),
            State::FactoryStep => self.eval_factory_step(),
            State::GotoStep => self.eval_goto_step(),
            State::SyncObjects => self.eval_sync_objects(),
            State::SyncStep => self.eval_sync_step(),
            State::Idle => self.eval_idle(),
        }
    }
}

impl Client {
    fn eval_init(&mut self) -> bool {
        if self.re.is_registered() {
            self.state = State::Auth
        } else {
            self.state = State::Register
        }

        true
    }
}

impl Client {
    fn init_register(&mut self, reg: &Registry) {
        self.send_register(&reg);
    }

    fn send_register(&mut self, reg: &Registry) {
        self.re.send_post_register(&reg, &mut self.end);
    }

    fn recv_register(&mut self, ev: &Event, reg: &Registry) {
        self.re
            .recv_post_register(&ev, &reg, &mut self.end, &self.varpath);
    }

    fn eval_register(&mut self) -> bool {
        if self.re.is_registered() {
            self.state = State::Auth;
            return true;
        }

        false
    }
}

impl Client {
    fn init_auth(&mut self, reg: &Registry) {
        self.send_auth(&reg);
    }

    fn send_auth(&mut self, reg: &Registry) {
        self.re.send_post_auth(&reg, &mut self.end);
    }

    fn recv_auth(&mut self, ev: &Event, reg: &Registry) {
        self.re.recv_post_auth(&ev, &reg, &mut self.end);
    }

    fn eval_auth(&mut self) -> bool {
        if self.re.is_authenticated() {
            if self.re.is_owned() {
                if self.pv.get_revs().is_factory_inited() {
                    self.state = State::GotoStep;
                } else {
                    self.state = State::LoadObjects;
                }
            } else {
                self.state = State::Claim;
            }
            return true;
        }

        false
    }
}

impl Client {
    fn init_claim(&mut self, reg: &Registry) {
        self.send_claim(&reg);
    }

    fn send_claim(&mut self, reg: &Registry) {
        self.re.send_get_device(&reg, &mut self.end);
    }

    fn recv_claim(&mut self, ev: &Event, reg: &Registry) {
        self.re
            .recv_get_device(&ev, &reg, &mut self.end, &self.varpath);
    }

    fn eval_claim(&mut self) -> bool {
        if self.re.is_owned() {
            self.re.reset_bearer();
            self.state = State::Auth;
            return true;
        }

        false
    }
}

impl Client {
    fn init_load_objects(&mut self, reg: &Registry) {
        self.send_load_objects(&reg);
    }

    fn send_load_objects(&mut self, reg: &Registry) {
        self.pc.send_get_objects(&reg, &mut self.end);
    }

    fn recv_load_objects(&mut self, ev: &Event, reg: &Registry) {
        if let Some(s) = self.pc.recv_get_objects(&mut self.end, &ev, &reg) {
            self.pv.set_objects(s);
        }
    }

    fn eval_load_objects(&mut self) -> bool {
        if self.pv.has_objects() {
            self.state = State::LoadStep;
            return true;
        }

        false
    }
}

impl Client {
    fn init_load_step(&mut self, reg: &Registry) {
        self.send_load_step(&reg);
    }

    fn send_load_step(&mut self, reg: &Registry) {
        self.pc.send_get_step(&reg, &mut self.end);
    }

    fn recv_load_step(&mut self, ev: &Event, reg: &Registry) {
        if let Some(s) = self.pc.recv_get_step(&mut self.end, &ev, &reg) {
            self.pv.get_spec_mut().parse(&s);
        }
    }

    fn eval_load_step(&mut self) -> bool {
        if self.pv.get_spec().has_raw() {
            self.state = State::FactoryStep;
            return true;
        }

        false
    }
}

impl Client {
    fn init_factory_step(&mut self, reg: &Registry) {
        self.pv.get_revs_mut().init_prefactory(&self.varpath);
        self.send_factory_step(&reg);
    }

    fn send_factory_step(&mut self, reg: &Registry) {
        self.pc.send_put_step(
            &self.pv.get_spec().get_raw(),
            &self.pv.get_revs().get_curr_local(),
            &reg,
            &mut self.end,
        );
    }

    fn recv_factory_step(&mut self, ev: &Event, reg: &Registry) {
        if self.pc.recv_put_step(&mut self.end, &ev, &reg) {
            self.pv.get_revs_mut().init_factory(&self.varpath);
        }
    }

    fn eval_factory_step(&mut self) -> bool {
        if self.pv.get_revs().is_factory_inited() {
            self.state = State::GotoStep;
            return true;
        }

        false
    }
}

impl Client {
    fn init_goto_step(&mut self, reg: &Registry) {
        self.send_goto_step(&reg);
    }

    fn send_goto_step(&mut self, reg: &Registry) {
        self.pc
            .send_goto_step(&self.pv.get_revs().get_curr_local(), &reg, &mut self.end);
        self.pc.send_get_devmeta(&reg, &mut self.end);
    }

    fn recv_goto_step(&mut self, ev: &Event, reg: &Registry) {
        self.pc.recv_goto_step(&mut self.end, &ev, &reg);
        if let Some(d) = self.pc.recv_get_devmeta(&mut self.end, &ev, &reg) {
            self.pv.get_devmeta_mut().parse(d);
        }
    }

    fn eval_goto_step(&mut self) -> bool {
        if self
            .pv
            .get_revs()
            .is_curr_synced(self.pv.get_devmeta().get_revision())
        {
            self.state = State::SyncObjects;
            return true;
        }

        false
    }
}

impl Client {
    fn init_sync_objects(&mut self, reg: &Registry) {
        let sizes = self.pv.get_objects();
        self.pv.get_spec_mut().prepare_objects_to_upload(sizes);
        self.send_sync_objects(&reg);
    }

    fn send_sync_objects(&mut self, reg: &Registry) {
        for o in self.pv.get_spec().get_objects_iter() {
            if !o.is_synced() && !o.has_puturl() {
                self.re.send_post_objects(
                    &o.get_sha256(),
                    &o.get_name(),
                    &o.get_size(),
                    &reg,
                    &mut self.end,
                );
            }
        }
    }

    fn recv_sync_objects(&mut self, ev: &Event, reg: &Registry) {
        for o in self.pv.get_spec_mut().get_objects_iter_mut() {
            // manage post response
            let status = self
                .re
                .recv_post_objects(&o.get_sha256(), &ev, &reg, &mut self.end);
            match status {
                ObjectStatus::Unknown => (),
                ObjectStatus::Synced => o.set_synced(true),
                ObjectStatus::Unsynced(p) => o.set_puturl(p),
            }
            // send put-get object request and manage response
            if o.has_puturl() {
                self.pc
                    .send_get_object_stream(&o.get_sha256(), &reg, &mut self.end);
                if let Some(iref) =
                    self.pc
                        .recving_get_object_stream(&o.get_sha256(), &mut self.end, &ev, &reg)
                {
                    self.re.send_put_object_stream(
                        &o.get_sha256(),
                        &o.get_puturl(),
                        &o.get_size(),
                        &reg,
                        &mut self.end,
                    );
                    if self.re.sending_put_object_stream(
                        &iref,
                        &o.get_sha256(),
                        &mut self.end,
                        &ev,
                        &reg,
                    ) {
                        self.pc.free_get_object_stream(&o.get_sha256());
                        self.re.free_put_object_stream(&o.get_sha256());
                        o.set_puturl("".to_string());
                    }
                }
            }
        }
    }

    fn eval_sync_objects(&mut self) -> bool {
        self.pv.get_spec_mut().clear_synced_objects();
        if !self.pv.get_spec().has_objects() {
            self.state = State::SyncStep;
            return true;
        }

        false
    }
}

impl Client {
    fn init_sync_step(&mut self, reg: &Registry) {
        self.send_sync_step(&reg);
    }

    fn send_sync_step(&mut self, reg: &Registry) {
        self.re
            .send_post_step(&self.pv.get_spec().get_raw(), &reg, &mut self.end);
    }

    fn recv_sync_step(&mut self, ev: &Event, reg: &Registry) {
        if self.re.recv_post_step(&ev, &reg, &mut self.end) {
            self.pv.get_spec_mut().clear();
        }
    }

    fn eval_sync_step(&mut self) -> bool {
        if !self.pv.get_spec().has_raw() {
            self.state = State::Idle;
            return true;
        }

        false
    }
}

impl Client {
    fn init_idle(&mut self, reg: &Registry) {
        self.send_idle(&reg);
    }

    fn send_idle(&mut self, reg: &Registry) {
        self.pc.send_get_devmeta(&reg, &mut self.end);
        if self.pv.get_devmeta().has_raw() {
            self.re
                .send_put_devmeta(&self.pv.get_devmeta().get_raw(), &reg, &mut self.end);
        }

        self.re.send_get_usrmeta(&reg, &mut self.end);
        if self.pv.get_usrmeta().has_raw() {
            self.pc
                .send_put_usrmeta(&self.pv.get_usrmeta().get_raw(), &reg, &mut self.end);
        }
    }

    fn recv_idle(&mut self, ev: &Event, reg: &Registry) {
        if let Some(d) = self.pc.recv_get_devmeta(&mut self.end, &ev, &reg) {
            self.pv.get_devmeta_mut().parse(d);
        }
        self.re.recv_put_devmeta(&ev, &reg, &mut self.end);

        if let Some(d) = self.re.recv_get_usrmeta(&ev, &reg, &mut self.end) {
            self.pv.get_usrmeta_mut().parse(d);
        }
        self.pc.recv_put_usrmeta(&mut self.end, &ev, &reg);
    }

    fn eval_idle(&mut self) -> bool {
        false
    }
}

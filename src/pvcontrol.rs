/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use mio::{event::Event, Registry};
use mio_httpc::CallRef;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::endpoint::Endpoint;

pub struct PVControl {
    config: Config,
    requests: HashMap<Request, CallRef>,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    host: String,
    port: u16,
    timeout: u64,
}

impl Config {
    pub fn new() -> Config {
        Config {
            host: "localhost".to_string(),
            port: 80,
            timeout: 5000,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
enum Request {
    GetObjects,
    GetStep,
    PutStep,
    GotoStep,
    GetObject(String),
    GetDevmeta,
    PutUsrmeta,
}

impl PVControl {
    pub fn new(config: Config) -> PVControl {
        PVControl {
            config,
            requests: HashMap::new(),
        }
    }
}

impl PVControl {
    pub fn send_get_objects(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GetObjects, end) {
            return;
        }

        let url = end.build_url_http(&self.config.host, self.config.port, "objects");
        if let Some(cref) = end.send_get(&url, &vec![], self.config.timeout, &reg) {
            self.requests.insert(Request::GetObjects, cref);
        }
    }

    pub fn send_get_step(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GetStep, end) {
            return;
        }

        let url = end.build_url_http(&self.config.host, self.config.port, "steps/current");
        if let Some(cref) = end.send_get(&url, &vec![], self.config.timeout, &reg) {
            self.requests.insert(Request::GetStep, cref);
        }
    }

    pub fn send_put_step(&mut self, spec: &str, rev: &str, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::PutStep, end) {
            return;
        }

        let mut path = "steps/".to_owned();
        path.push_str(&rev);
        let url = end.build_url_http(&self.config.host, self.config.port, &path);
        if let Some(cref) = end.send_put(&url, &vec![], &spec, self.config.timeout, &reg) {
            self.requests.insert(Request::PutStep, cref);
        }
    }

    pub fn send_goto_step(&mut self, rev: &str, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GotoStep, end) {
            return;
        }

        let url = end.build_url_http(&self.config.host, self.config.port, "commands");
        let body = self.build_body_command("LOCAL_RUN", rev);
        if let Some(cref) = end.send_post(&url, &vec![], &body, self.config.timeout, &reg) {
            self.requests.insert(Request::GotoStep, cref);
        }
    }

    pub fn send_get_object_stream(&mut self, sha256: &str, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GetObject(sha256.to_string()), end) {
            return;
        }

        let mut path = "objects/".to_owned();
        path.push_str(&sha256);
        let url = end.build_url_https(&self.config.host, self.config.port, &path);

        if let Some(cref) = end.send_get_stream(&url, &vec![], self.config.timeout, &reg) {
            self.requests
                .insert(Request::GetObject(sha256.to_string()), cref.clone());
        }
    }

    pub fn send_get_devmeta(&mut self, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::GetDevmeta, end) {
            return;
        }

        let url = end.build_url_http(&self.config.host, self.config.port, "device-meta");
        if let Some(cref) = end.send_get(&url, &vec![], self.config.timeout, &reg) {
            self.requests.insert(Request::GetDevmeta, cref);
        }
    }

    pub fn send_put_usrmeta(&mut self, usrmeta: &str, reg: &Registry, end: &mut Endpoint) {
        if self.is_pending(&Request::PutUsrmeta, end) {
            return;
        }

        let url = end.build_url_http(&self.config.host, self.config.port, "user-meta");
        if let Some(cref) = end.send_put(&url, &vec![], &usrmeta, self.config.timeout, &reg) {
            self.requests.insert(Request::PutUsrmeta, cref);
        }
    }

    fn is_pending(&mut self, req: &Request, end: &mut Endpoint) -> bool {
        if let Some(&cref) = self.requests.get(&req) {
            return end.is_pending(&cref);
        }
        false
    }

    fn build_body_command(&self, op: &str, payload: &str) -> String {
        #[derive(Serialize)]
        struct Command {
            op: String,
            payload: String,
        }

        let command = Command {
            op: op.to_string(),
            payload: payload.to_string(),
        };

        match serde_json::to_string(&command) {
            Ok(b) => return b,
            Err(e) => println!("could not serialize command body: {}", e),
        };

        "".to_string()
    }
}

impl PVControl {
    pub fn recv_get_objects(
        &mut self,
        end: &mut Endpoint,
        ev: &Event,
        reg: &Registry,
    ) -> Option<HashMap<String, String>> {
        #[derive(Deserialize, Debug)]
        pub struct ListedObject {
            sha256: String,
            size: String,
        }
        if let Some(&cref) = self.requests.get(&Request::GetObjects) {
            if let Some((r, b)) = end.recv(&ev, &reg, &cref) {
                if r == 200 {
                    match serde_json::from_slice(&b) {
                        Ok(s) => {
                            let mut objects: HashMap<String, String> = HashMap::new();
                            let mut l: Vec<ListedObject> = s;
                            for lo in l.drain(..) {
                                objects.insert(lo.sha256, lo.size);
                            }
                            return Some(objects);
                        }
                        Err(e) => println!("could not parse objects: {}", e),
                    }
                }
                self.requests.remove(&Request::GetObjects);
            }
        }
        None
    }

    pub fn recv_get_step(
        &mut self,
        end: &mut Endpoint,
        ev: &Event,
        reg: &Registry,
    ) -> Option<String> {
        if let Some(&cref) = self.requests.get(&Request::GetStep) {
            if let Some((r, b)) = end.recv(&ev, &reg, &cref) {
                if r == 200 {
                    match String::from_utf8(b.clone()) {
                        Ok(s) => return Some(s),
                        Err(e) => println!("could not get step: {}", e),
                    }
                }
                self.requests.remove(&Request::GetStep);
            }
        }
        None
    }

    pub fn recv_put_step(&mut self, end: &mut Endpoint, ev: &Event, reg: &Registry) -> bool {
        if let Some(&cref) = self.requests.get(&Request::PutStep) {
            if let Some((r, _b)) = end.recv(&ev, &reg, &cref) {
                if r == 200 {
                    return true;
                }
                self.requests.remove(&Request::PutStep);
            }
        }
        false
    }

    pub fn recv_goto_step(&mut self, end: &mut Endpoint, ev: &Event, reg: &Registry) {
        if let Some(&cref) = self.requests.get(&Request::GotoStep) {
            if let Some((_r, _b)) = end.recv(&ev, &reg, &cref) {
                self.requests.remove(&Request::GotoStep);
            }
        }
    }

    pub fn recving_get_object_stream(
        &mut self,
        sha256: &str,
        end: &mut Endpoint,
        ev: &Event,
        reg: &Registry,
    ) -> Option<CallRef> {
        if let Some(cref) = self.requests.get(&Request::GetObject(sha256.to_string())) {
            if end.recving_stream(&ev, &reg, &cref) {
                return Some(cref.clone());
            }
        }
        None
    }

    pub fn free_get_object_stream(&mut self, sha256: &str) {
        self.requests
            .remove(&Request::GetObject(sha256.to_string()));
    }

    pub fn recv_get_devmeta(
        &mut self,
        end: &mut Endpoint,
        ev: &Event,
        reg: &Registry,
    ) -> Option<String> {
        if let Some(&cref) = self.requests.get(&Request::GetDevmeta) {
            if let Some((r, b)) = end.recv(&ev, &reg, &cref) {
                if r == 200 {
                    match String::from_utf8(b.clone()) {
                        Ok(s) => return Some(s),
                        Err(e) => println!("could not get devmeta: {}", e),
                    }
                }
                self.requests.remove(&Request::GetDevmeta);
            }
        }
        None
    }

    pub fn recv_put_usrmeta(&mut self, end: &mut Endpoint, ev: &Event, reg: &Registry) {
        if let Some(&cref) = self.requests.get(&Request::PutUsrmeta) {
            if let Some((_r, _b)) = end.recv(&ev, &reg, &cref) {
                self.requests.remove(&Request::GetDevmeta);
            }
        }
    }
}

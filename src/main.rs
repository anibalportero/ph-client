/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use mio::{Events, Interest, Poll, Token};
use mio_timerfd::{ClockId, TimerFd};
use std::process::exit;
use std::time::Duration;

use crate::core::Client;
use crate::disk::Config;

mod core;
mod disk;
mod endpoint;
mod pantavisor;
mod pvcontrol;
mod remote;

const PERIODIC: Token = Token(0);

fn main() {
    let paths = load_args();
    paths.print();

    let mut config = Config::new();

    // TODO: configure logs here

    config.load(&paths.config);
    config.print();

    let mut client = Client::new(config.remote, config.pvcontrol, &paths.var);

    let mut poll = match Poll::new() {
        Ok(p) => p,
        Err(e) => {
            println!("could not init poll: {}", e);
            exit(1);
        }
    };
    let timeout = Duration::from_millis(100);
    let mut events = Events::with_capacity(8);
    let mut timer = match TimerFd::new(ClockId::Monotonic) {
        Ok(t) => t,
        Err(e) => {
            println!("could not init timer: {}", e);
            exit(1);
        }
    };

    if let Err(e) = timer.set_timeout_interval(&Duration::from_millis(5000)) {
        println!("could not set timeout to timer: {}", e);
        exit(1);
    }
    if let Err(e) = poll
        .registry()
        .register(&mut timer, PERIODIC, Interest::READABLE)
    {
        println!("could not register timer in poll: {}", e);
        exit(1);
    }

    loop {
        client.init_state(&poll.registry());

        loop {
            if let Err(e) = poll.poll(&mut events, Some(timeout)) {
                println!("could not poll: {}", e);
            }

            client.check_timeout();

            for event in events.iter() {
                match event.token() {
                    PERIODIC => match timer.read() {
                        Ok(_) => client.send_state(&poll.registry()),
                        Err(e) => println!("could not read timer: {}", e),
                    },
                    _ => {
                        client.recv_state(&event, &poll.registry());
                    }
                }
            }

            // process this state until state transition
            if client.eval_state() {
                break;
            }
        }
    }
}

#[derive(Debug)]
struct Paths {
    config: String,
    var: String,
}

impl Paths {
    fn new() -> Paths {
        Paths {
            config: "/etc/ph-client/default.json".to_string(),
            var: "/var/ph-client/".to_string(),
        }
    }

    fn print(&self) {
        println!("{:?}", self);
    }
}

fn load_args() -> Paths {
    let mut paths = Paths::new();
    let mut args: Vec<String> = std::env::args().collect();
    if args.is_empty() {
        return paths;
    }

    args.remove(0);
    loop {
        if args.is_empty() {
            break;
        }

        let option = args.remove(0);
        match option.as_str() {
            "-h" => {
                print_help();
                exit(0);
            }
            "-c" => {
                if !args.is_empty() {
                    paths.config = args.remove(0);
                } else {
                    println!("no path provided after -c");
                    print_help();
                    exit(1);
                }
            }
            "-v" => {
                if !args.is_empty() {
                    paths.var = args.remove(0);
                } else {
                    println!("no path provided after -v");
                    print_help();
                    exit(1);
                }
            }
            _ => {
                println!("wrong argument");
                print_help();
                exit(1);
            }
        }
    }
    paths
}

fn print_help() {
    println!("Usage: ph-client [options]");
    println!("A Pantacor Hub client");
    println!("options:");
    println!("    -h    show help");
    println!("    -c    <config-file-path>");
    println!("    -v    <var-files-path>");
}

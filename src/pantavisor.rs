/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

pub struct Pantavisor {
    objects: HashMap<String, String>,
    spec: Spec,
    revs: Revisions,
    devmeta: Devmeta,
    usrmeta: Usrmeta,
}

impl Pantavisor {
    pub fn new(varpath: &str) -> Pantavisor {
        let mut revs = Revisions::new();
        revs.load(&varpath);
        Pantavisor {
            objects: HashMap::new(),
            spec: Spec::new(),
            revs,
            devmeta: Devmeta::new(),
            usrmeta: Usrmeta::new(),
        }
    }

    pub fn set_objects(&mut self, objects: HashMap<String, String>) {
        self.objects = objects;
    }

    pub fn get_objects(&mut self) -> HashMap<String, String> {
        let mut objects: HashMap<String, String> = HashMap::new();
        for (k, v) in self.objects.drain() {
            objects.insert(k, v);
        }
        objects
    }

    pub fn has_objects(&self) -> bool {
        !self.objects.is_empty()
    }

    pub fn get_spec(&self) -> &Spec {
        &self.spec
    }

    pub fn get_spec_mut(&mut self) -> &mut Spec {
        &mut self.spec
    }

    pub fn get_revs(&self) -> &Revisions {
        &self.revs
    }

    pub fn get_revs_mut(&mut self) -> &mut Revisions {
        &mut self.revs
    }

    pub fn get_devmeta(&self) -> &Devmeta {
        &self.devmeta
    }

    pub fn get_devmeta_mut(&mut self) -> &mut Devmeta {
        &mut self.devmeta
    }

    pub fn get_usrmeta(&self) -> &Usrmeta {
        &self.usrmeta
    }

    pub fn get_usrmeta_mut(&mut self) -> &mut Usrmeta {
        &mut self.usrmeta
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Revision {
    local: String,
    remote: u32,
}

impl Revision {
    pub fn new(rev: u32) -> Revision {
        let mut local = "locals/ph_client_".to_string();
        local.push_str(&rev.to_string());
        Revision { local, remote: rev }
    }
}

#[derive(Deserialize, Debug)]
pub struct Spec {
    raw: String,
    objects: Vec<Object>,
}

impl Spec {
    pub fn new() -> Spec {
        Spec {
            raw: "".to_string(),
            objects: Vec::new(),
        }
    }

    pub fn clear(&mut self) {
        self.raw = "".to_string();
        self.objects = Vec::new();
    }

    pub fn parse(&mut self, spec: &str) {
        match serde_json::from_str(spec) {
            Ok(b) => {
                let mut full_spec: HashMap<String, Value>;
                full_spec = b;
                // get objects from spec
                for (k, v) in full_spec.drain() {
                    if k != "#spec" {
                        if let Some(s) = v.as_str() {
                            self.objects.push(Object::new(s.to_string(), k));
                        }
                    }
                }
                // get raw
                self.raw = spec.to_string();
            }
            Err(e) => println!("could not parse spec: {}", e),
        }
    }

    pub fn get_raw(&self) -> &str {
        &self.raw
    }

    pub fn has_raw(&self) -> bool {
        self.raw.len() > 0
    }

    pub fn get_objects_iter(&self) -> std::slice::Iter<'_, Object> {
        self.objects.iter()
    }

    pub fn get_objects_iter_mut(&mut self) -> std::slice::IterMut<'_, Object> {
        self.objects.iter_mut()
    }

    pub fn has_objects(&self) -> bool {
        !self.objects.is_empty()
    }

    pub fn prepare_objects_to_upload(&mut self, mut sizes: HashMap<String, String>) {
        for o in self.objects.iter_mut() {
            if let Some(s) = sizes.remove(&o.sha256) {
                o.set_size(s);
            } else {
                println!("object {} could not be found in objects", o.sha256);
            }
        }
    }

    pub fn clear_synced_objects(&mut self) {
        self.objects.retain(|o| {
            if o.synced {
                return false;
            }
            true
        });
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Object {
    sha256: String,
    name: String,
    size: String,
    puturl: String,
    synced: bool,
}

impl Object {
    pub fn new(sha256: String, name: String) -> Object {
        Object {
            sha256,
            name,
            size: "".to_string(),
            puturl: "".to_string(),
            synced: false,
        }
    }

    pub fn get_sha256(&self) -> &str {
        &self.sha256
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn set_size(&mut self, size: String) {
        self.size = size;
    }

    pub fn get_size(&self) -> &str {
        &self.size
    }

    pub fn set_puturl(&mut self, puturl: String) {
        self.puturl = puturl;
    }

    pub fn get_puturl(&self) -> &str {
        &self.puturl
    }

    pub fn has_puturl(&self) -> bool {
        !self.puturl.is_empty()
    }

    pub fn set_synced(&mut self, synced: bool) {
        self.synced = synced;
    }

    pub fn is_synced(&self) -> bool {
        self.synced
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Revisions {
    done: Revision,
    curr: Revision,
    next: Revision,
}

impl Revisions {
    pub fn new() -> Revisions {
        Revisions {
            done: Revision::new(0),
            curr: Revision::new(0),
            next: Revision::new(0),
        }
    }

    pub fn init_prefactory(&mut self, varpath: &str) {
        self.done = Revision::new(0);
        self.curr = Revision::new(0);
        self.next = Revision::new(0);
        self.save(&varpath);
    }

    pub fn init_factory(&mut self, varpath: &str) {
        self.done = Revision::new(0);
        self.curr = Revision::new(0);
        self.next = Revision::new(1);
        self.save(&varpath);
    }

    pub fn is_factory_inited(&self) -> bool {
        !(self.done.remote == 0 && self.curr.remote == 0 && self.next.remote == 0)
    }

    pub fn get_curr_local(&self) -> &str {
        &self.curr.local
    }

    pub fn is_curr_synced(&self, rev: &str) -> bool {
        match rev.parse::<u32>() {
            Ok(r) => {
                if r == self.curr.remote {
                    return true;
                }
            }
            Err(e) => println!("could not parse remote revision: {}", e),
        }
        false
    }
}

pub struct Devmeta {
    raw: String,
    revision: String,
}

impl Devmeta {
    pub fn new() -> Devmeta {
        Devmeta {
            raw: "".to_string(),
            revision: "".to_string(),
        }
    }

    pub fn parse(&mut self, devmeta: String) {
        #[derive(Deserialize)]
        struct KnownDevmeta {
            #[serde(rename = "pantavisor.revision")]
            revision: String,
        }
        match serde_json::from_str(&devmeta) {
            Ok(d) => {
                let known_devmeta: KnownDevmeta;
                known_devmeta = d;
                self.revision = known_devmeta.revision;
                self.raw = devmeta;
            }
            Err(e) => println!("could not parse devmeta: {}", e),
        }
    }

    pub fn get_raw(&self) -> &str {
        &self.raw
    }

    pub fn has_raw(&self) -> bool {
        &self.raw.len() > &0
    }

    pub fn get_revision(&self) -> &str {
        &self.revision
    }
}

pub struct Usrmeta {
    raw: String,
}

impl Usrmeta {
    pub fn new() -> Usrmeta {
        Usrmeta {
            raw: "".to_string(),
        }
    }

    pub fn parse(&mut self, usrmeta: String) {
        self.raw = usrmeta;
    }

    pub fn get_raw(&self) -> &str {
        &self.raw
    }

    pub fn has_raw(&self) -> bool {
        &self.raw.len() > &0
    }
}
